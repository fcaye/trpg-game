﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTracker : MonoBehaviour {

    #region Actions
    //Triggers when audiosource isPlaying changes to true
    public Action<AudioTracker> onPlay;

    //Triggers when audiosource isPlayig changes to false without completing
    public Action<AudioTracker> onPause;

    //Triggers when audiosource isPlaying changes to false (stop or end)
    public Action<AudioTracker> onComplete;

    //Triggers when audiosource repeats
    public Action<AudioTracker> onLoop;
    #endregion

    #region Fields & Properties
    //if true will stop tracking audiosource when it stops
    public bool autoStop = false;

    //Source that the component is tracking
    public AudioSource source { get; private set; }

    //Last tracked time of audiosource
    private float lastTime;

    //Last tracked value of isPlaying
    private bool lastIsPlaying;

    const string trackingCoroutine = "TrackSequence";
    #endregion

    #region Public
    public void Track(AudioSource source)
    {
        Cancel();
        this.source = source;
        if(source != null)
        {
            lastTime = source.time;
            lastIsPlaying = source.isPlaying;
            StartCoroutine(trackingCoroutine);
        }
    }

    public void Cancel()
    {
        StopCoroutine(trackingCoroutine);
    }
    #endregion

    #region Private
    IEnumerator TrackSequence()
    {
        while(true)
        {
            yield return null;
            SetTime(source.time);
            SetIsPlaying(source.isPlaying);
        }
    }

    void AudioSourceBegan()
    {
        if(onPlay != null)
        {
            onPlay(this);
        }
    }

    void AudioSourceLooped()
    {
        if(onLoop != null)
        {
            onLoop(this);
        }
    }

    void AudioSourceCompleted()
    {
        if(onComplete != null)
        {
            onComplete(this);
        }
    }

    void AudioSourcePaused()
    {
        if(onPause != null)
        {
            onPause(this);
        }
    }

    void SetIsPlaying(bool isPlaying)
    {
        if(lastIsPlaying == isPlaying)
        {
            return;
        }

        lastIsPlaying = isPlaying;

        if(isPlaying)
        {
            AudioSourceBegan();
        }
        else if(Mathf.Approximately(source.time, 0))
        {
            AudioSourceCompleted();
        }
        else
        {
            AudioSourcePaused();
        }

        if(isPlaying == false && autoStop == true)
        {
            StopCoroutine(trackingCoroutine);
        }
    }

    void SetTime(float time)
    {
        if(lastTime > time)
        {
            AudioSourceLooped();
        }
        lastTime = time;
    }
    #endregion
}
