﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAbilityEffect : BaseAbilityEffect {

    #region Public
    public override int Predict(Tile target)
    {
        Unit attacker = GetComponentInParent<Unit>();
        Unit defender = target.content.GetComponent<Unit>();

        //Get the attackers base ATK stat
        //Consider items / support check / status check / equipment ... etc
        int attack = GetStat(attacker, defender, GetAttackNotification, 0);

        //Get target base DEF stat
        //Consider items / support check / status check / equipment ... etc
        int defense = GetStat(attacker, defender, GetDefenseNotification, 0);

        //Calculate base damage
        int damage = attack - (defense / 2);
        damage = Mathf.Max(damage, 1);

        //Get abilities power stat consider variations
        int power = GetStat(attacker, defender, GetPowerNotification, 0);

        //Apply power bonus
        damage = power * damage / 100;
        damage = Mathf.Max(damage, 1);

        //Tweak damage based on variety of other checks
        //Elemental damage / Critical Hits / Damage Multipliers / etc.
        damage = GetStat(attacker, defender, TweakDamageNotification, damage);

        //Clamp damage value to range
        damage = Mathf.Clamp(damage, minDamage, maxDamage);

        return -damage;
    }

    protected override int OnApply(Tile target)
    {
        Unit defender = target.content.GetComponent<Unit>();

        //Start with predicted damage value
        int value = Predict(target);

        //Add random variance +-10%
        value = Mathf.FloorToInt(value * UnityEngine.Random.Range(0.9f, 1.1f));

        //Clamp damage to a range
        value = Mathf.Clamp(value, minDamage, maxDamage);

        //Apply damage to target
        Stats s = defender.GetComponent<Stats>();
        s[StatTypes.HP] += value;
        return value;
    }
    #endregion
}
