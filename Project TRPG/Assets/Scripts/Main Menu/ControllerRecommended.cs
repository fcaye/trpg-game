﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ControllerRecommended : MonoBehaviour {

    public Sprite controllerConnected;
    public Sprite controllerRecommended;


    private Image controllerImage;
    private AudioSource controllerRecommendedAS;

	// Use this for initialization
	void Start () {
        controllerImage = gameObject.GetComponent<Image>();
        controllerRecommendedAS = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void LateUpdate()
    {
        if(CheckForJoysticks())
        {
            controllerImage.sprite = controllerConnected;

        }
        else
        {
            controllerImage.sprite = controllerRecommended;
            //controllerRecommendedAS.Play();
        }
    }

    bool CheckForJoysticks()
    {
        string[] joysticks = Input.GetJoystickNames();

        for(int i = 0; i < joysticks.Length; i++)
        {
            //Debug.Log(joysticks[i]);

            if(joysticks[i].Length > 5)
            {
                
                return true;
            }
        }

        controllerRecommendedAS.Play();
        return false;
    }
}
