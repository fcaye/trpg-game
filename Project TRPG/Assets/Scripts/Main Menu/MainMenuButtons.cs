﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {

    public GameObject mainPanel;
    public GameObject exitPanel;
    public Button exitPanelButton;
    public Button noExitButton;

    public AudioClip fxClick;
    public AudioClip fxPositive;
    public AudioClip fxBack;

    AudioSource menuAudioSource;

    private void Awake()
    {
        menuAudioSource = gameObject.GetComponent<AudioSource>();
    }

    public void GoToScene(string sceneName)
    {
        StartCoroutine(TransitionToScene(sceneName));
    }

    IEnumerator TransitionToScene(string sceneName)
    {
        menuAudioSource.clip = fxPositive;
        menuAudioSource.Play();

        yield return new WaitForSeconds(0.15f);

        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    public void ShowExitPanel()
    {
        menuAudioSource.clip = fxPositive;
        menuAudioSource.Play();
        mainPanel.SetActive(false);
        exitPanel.SetActive(true);
        noExitButton.Select();
    }

    public void ConfirmExit()
    {
        StartCoroutine(QuitTransition());
    }

    IEnumerator QuitTransition()
    {
        menuAudioSource.clip = fxBack;
        menuAudioSource.Play();

        yield return new WaitForSeconds(1f);

        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public void CancelExit()
    {
        menuAudioSource.clip = fxBack;
        menuAudioSource.Play();
        mainPanel.SetActive(true);
        exitPanel.SetActive(false);
        exitPanelButton.Select();
    }
}
