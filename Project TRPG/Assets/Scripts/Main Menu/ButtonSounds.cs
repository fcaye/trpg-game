﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonSounds : MonoBehaviour, ISelectHandler {

    AudioSource buttonSource;

    public AudioClip fxClick;
    public AudioClip fxPositive;
    public AudioClip fxBack;

	// Use this for initialization
	void Start () {
        buttonSource = gameObject.GetComponent<AudioSource>();
	}

    public void OnSelect(BaseEventData eventData)
    {
        Debug.Log(this.gameObject.name + " was selected");

        buttonSource.clip = fxClick;
        buttonSource.Play();
    }
}
