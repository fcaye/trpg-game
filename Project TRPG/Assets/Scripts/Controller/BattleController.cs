﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : StateMachine {

    public CameraRig cameraRig;
    public Board board;
    public LevelData levelData;
    public Transform tileSelectionIndicator;
    public Point pos;
    public Tile currentTile { get { return board.GetTile(pos); } }
    public AbilityMenuPanelController abilityMenuPanelController;
    public Turn turn = new Turn();
    public List<Unit> units = new List<Unit>();
    public IEnumerator round;
    public StatPanelController statPanelController;
    public HitSuccessIndicator hitSuccessIndicator;
    public FacingIndicator facingIndicator;
    public BattleMessageController battleMessageController;
    public ComputerPlayer cpu;

    public ConversationData introCutscene;
    public ConversationData victoryCutscene;
    public ConversationData defeatCutscene;

    public enum StatesList
    {
        AbilityTarget,
        ActionSelection,
        BaseAbility,
        CategorySelection,
        CommandSelection,
        ConfirmAbilityTarget,
        CutScene,
        EndBattle,
        EndFacing, 
        Explore,
        InitBattle,
        MoveSequence,
        MoveTarget,
        PauseBattle,
        PerformAbility,
        SelectUnit
    }

    public StatesList myPreviousState;

    void Start()
    {
        ChangeState<InitBattleState>();
    }

    private void Update()
    {
        if(Input.GetButtonUp("Start"))
        {
            if (_currentState is AbilityTargetState)
            {
                myPreviousState = StatesList.AbilityTarget;
            }
            if (_currentState is ActionSelectionState)
            {
                myPreviousState = StatesList.ActionSelection;
            }
            if (_currentState is BaseAbilityMenuState)
            {
                myPreviousState = StatesList.BaseAbility;
            }
            if (_currentState is CategorySelectionState)
            {
                myPreviousState = StatesList.CategorySelection;
            }
            if (_currentState is CommandSelectionState)
            {
                myPreviousState = StatesList.CommandSelection;
            }
            if (_currentState is ConfirmAbilityTargetState)
            {
                myPreviousState = StatesList.ConfirmAbilityTarget;
            }
            if (_currentState is CutSceneState)
            {
                myPreviousState = StatesList.CutScene;
            }
            if (_currentState is EndBattleState)
            {
                myPreviousState = StatesList.EndBattle;
            }
            if (_currentState is EndFacingState)
            {
                myPreviousState = StatesList.EndFacing;
            }
            if (_currentState is ExploreState)
            {
                myPreviousState = StatesList.Explore;
            }
            if (_currentState is InitBattleState)
            {
                myPreviousState = StatesList.InitBattle;
            }
            if (_currentState is MoveSequenceState)
            {
                myPreviousState = StatesList.MoveSequence;
            }
            if (_currentState is MoveTargetState)
            {
                myPreviousState = StatesList.MoveTarget;
            }
            if (_currentState is PerformAbilityState)
            {
                myPreviousState = StatesList.PerformAbility;
            }
            if (_currentState is SelectUnitState)
            {
                myPreviousState = StatesList.SelectUnit;
            }

            ChangeState<PauseBattleState>();
        }
    }
}
