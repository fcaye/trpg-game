﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Party = System.Collections.Generic.List<UnityEngine.GameObject>;

public static class ExperienceManager {

    const float minLevelBonus = 1.5f;
    const float maxLevelBonus = 0.5f;

    public static void AwardExperience(int amount, Party party)
    {
        //Grab list of rank components from party
        List<Rank> ranks = new List<Rank>(party.Count);
        for(int i = 0; i < party.Count; ++i)
        {
            Rank r = party[i].GetComponent<Rank>();
            if(r != null)
            {
                ranks.Add(r);
            }
        }

        //Step 1: determine range in actor level stats
        int min = int.MaxValue;
        int max = int.MinValue;
        for(int i = ranks.Count - 1; i >= 0; --i)
        {
            min = Mathf.Min(ranks[i].LVL, min);
            max = Mathf.Max(ranks[i].LVL, max);
        }

        //Step 2: weight amount to award per actor based on levels
        float[] weights = new float[party.Count];
        float summedWeights = 0;
        for(int i = ranks.Count - 1; i >= 0; --i)
        {
            float percent = (float)(ranks[i].LVL - min) / (float)(max - min);
            weights[i] = Mathf.Lerp(minLevelBonus, maxLevelBonus, percent);
            summedWeights += weights[i];
        }

        //Step 3: hand out weighted award
        for(int i = ranks.Count - 1; i >= 0; --i)
        {
            int subAmount = Mathf.FloorToInt((weights[i] / summedWeights) * amount);
            ranks[i].EXP += subAmount;
        }
    }
}
