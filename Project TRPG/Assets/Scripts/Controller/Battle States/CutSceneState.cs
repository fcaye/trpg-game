﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutSceneState : BattleState {

    ConversationController conversationController;
    ConversationData data;
    ConversationData startingCutscene;
    ConversationData defeatCutscene;
    ConversationData victoryCutscene;

    protected override void Awake()
    {
        base.Awake();
        conversationController = owner.GetComponentInChildren<ConversationController>();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();
        if (data)
        {

        }
            //Resources.UnloadAsset(data);
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("CutSceneState Enter");
        if(IsBattleOver())
        {
            if(DidPlayerWin())
            {
                data = owner.victoryCutscene;
            }
            else
            {
                data = owner.defeatCutscene;
            }
        }
        else
        {
            data = owner.introCutscene;
        }
        conversationController.Show(data);
    }

    public override void Exit()
    {
        base.Exit();
        Debug.Log("CutSceneState Exit()");
        if(data)
        {
            Resources.UnloadAsset(data);
        }
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        ConversationController.completeEvent += OnCompleteConversation;
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        ConversationController.completeEvent -= OnCompleteConversation;
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        base.OnFire(sender, e);
        conversationController.Next();
    }

    void OnCompleteConversation(object sender, System.EventArgs e)
    {
        if(IsBattleOver())
        {
            owner.ChangeState<EndBattleState>();
        }
        else
        {
            owner.ChangeState<SelectUnitState>();
        }
    }
}
