﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseBattleState : BattleState {

    PauseMenuController pauseMenuController;
    //State previousState;

    protected override void Awake()
    {
        base.Awake();
        //Debug.Log("Pause Battle State Awake");
        pauseMenuController = owner.GetComponentInChildren<PauseMenuController>();
        //previousState = owner.previousState;
    }

    public override void Enter()
    {
        base.Enter();
        Debug.Log("Pause Battle State Enter");
        pauseMenuController.ShowPausePanel();
    }

    private void LateUpdate()
    {
        if (pauseMenuController.continuePressed)
        {
            Debug.Log("Previous state: " + owner.myPreviousState);
            pauseMenuController.continuePressed = false;
            switch (owner.myPreviousState)
            {
                case BattleController.StatesList.AbilityTarget:
                    owner.ChangeState<AbilityTargetState>();
                    break;
                case BattleController.StatesList.ActionSelection:
                    owner.ChangeState<ActionSelectionState>();
                    break;
                case BattleController.StatesList.BaseAbility:
                    owner.ChangeState<BaseAbilityMenuState>();
                    break;
                case BattleController.StatesList.CategorySelection:
                    owner.ChangeState<CategorySelectionState>();
                    break;
                case BattleController.StatesList.CommandSelection:
                    owner.ChangeState<CommandSelectionState>();
                    break;
                case BattleController.StatesList.ConfirmAbilityTarget:
                    owner.ChangeState<ConfirmAbilityTargetState>();
                    break;
                case BattleController.StatesList.CutScene:
                    owner.ChangeState<CutSceneState>();
                    break;
                case BattleController.StatesList.EndBattle:
                    owner.ChangeState<EndBattleState>();
                    break;
                case BattleController.StatesList.EndFacing:
                    owner.ChangeState<EndFacingState>();
                    break;
                case BattleController.StatesList.Explore:
                    owner.ChangeState<ExploreState>();
                    break;
                case BattleController.StatesList.InitBattle:
                    owner.ChangeState<InitBattleState>();
                    break;
                case BattleController.StatesList.MoveSequence:
                    owner.ChangeState<MoveSequenceState>();
                    break;
                case BattleController.StatesList.MoveTarget:
                    owner.ChangeState<MoveTargetState>();
                    break;
                case BattleController.StatesList.PauseBattle:
                    owner.ChangeState<PauseBattleState>();
                    break;
                case BattleController.StatesList.PerformAbility:
                    owner.ChangeState<PerformAbilityState>();
                    break;
                case BattleController.StatesList.SelectUnit:
                    owner.ChangeState<SelectUnitState>();
                    break;
                default:
                    break;
            }
        }
    }
    protected override void AddListeners()
    {
        base.AddListeners();
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        base.OnFire(sender, e);
    }

    public override void Exit()
    {
        base.Exit();
        Debug.Log("PauseBattleState Exit()");
    }
}
