﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour {

    const string showKey = "Show";
    const string hideKey = "Hide";

    Canvas canvas;

    public Button continueButton;
    public Button quitButton;
    public bool continuePressed = false;
    public Panel pauseMenuPanel;

    private void Awake()
    {
        pauseMenuPanel.gameObject.SetActive(false);
    }

    void Start()
    {
        //canvas.GetComponentInChildren<Canvas>();
        //canvas.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        //continuePressed = false;
        //continueButton.Select();
    }

    public void ShowPausePanel()
    {
        pauseMenuPanel.gameObject.SetActive(true);
        continueButton.Select();
        continuePressed = false;
    }

    public void ExitButtonPressed()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void ContinueButtonPressed()
    {
        continuePressed = true;
        pauseMenuPanel.gameObject.SetActive(false);
    }
}
