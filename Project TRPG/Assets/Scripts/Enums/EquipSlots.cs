﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum EquipSlots {

	None = 0,
    Primary = 1 << 0,       //Usually a weapon (left hand ?)
    Secondary = 1 << 1,     //Usually a shield (right hand?)
    Head = 1 << 2,          //Helmet / hat / something
    Body = 1 << 3,          //Body armor / chestplate 
    Accessory = 1 << 4      //Ring / belt / pendant
}
